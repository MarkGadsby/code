void HumanPlay(int);
void ComputerPlay(int);
void SetInitialGameState(char*, int, int);
void SetTargetGameState(char*, int, int);
bool MakeFrogMove(char*, int);
bool MakeToadMove(char*, int);

class GameStateNode
{
public:
	GameStateNode(char*, int, char);
	~GameStateNode();

	char* m_pGameState;
	int m_playStripSize;
	char m_Move;

	GameStateNode* m_pFrogBranch;
	GameStateNode* m_pToadBranch;
	GameStateNode* m_pParent;

	bool m_IsFrogBranchOpen;
	bool m_IsToadBranchOpen;

	void IsDeadEnd(GameStateNode* pBranchSendingMessage);
};

bool AddNewMoves(GameStateNode*);
GameStateNode* FindOpenNode(GameStateNode*);
void SearchTreeForGameState(GameStateNode*, char*);
void DisplayPathToSolution(GameStateNode*);
