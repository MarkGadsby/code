#include <string>

using namespace std;

class Node
{
public:
    Node(string newWord);
    ~Node();

    string  m_sWord;
    short   m_nInstances;
    Node*   m_pLeft;
    Node*   m_pRight;
};
