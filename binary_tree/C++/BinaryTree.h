#include "Node.h"
#include <string>

class BinaryTree
{
    private:
        Node* pRoot;
        
        Node* insert(Node* pRoot, string newWord);
        void traverse_in_order(Node* pRoot);
        void traverse_in_order(Node* pRoot, ofstream* pSortedFile);
        void traverse_in_order(Node* pRoot, string* pArray, int* pIndex);
        void count_nodes(Node* pRoot, int* pCounter);
        int  search(Node* pRoot, string item_sought);

    public:
        BinaryTree();
        ~BinaryTree();

        void AddToTree(string newWord);
        int FindInTree(string newWord);
        void DisplayTreeInOrder();
        int GetNumberOfNodes();
        void ArrayFromTree(string* pArray, int* pIndex);
        void PrintTreeToFile(ofstream* pSortedFile);
};