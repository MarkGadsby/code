#include "Shakespeare.h"
#include <fstream>
#include <iostream>

// Constructor
WorksOfShakespeare::WorksOfShakespeare()
{
    pBinaryTree = new BinaryTree();
}

// Destructor
WorksOfShakespeare::~WorksOfShakespeare()
{
    delete pBinaryTree;
    delete[] pArray;
}

string* WorksOfShakespeare::GetArray()
{
    return pArray;
}

int WorksOfShakespeare::FindInTree(string item_sought)
{
    return pBinaryTree->FindInTree(item_sought);
}

void WorksOfShakespeare::BuildTreeFromWorks()
{
    ifstream readFile;
    readFile.open("./test_data/Shakespeare.txt");

    string singleWord;

    while (readFile >> singleWord)
    {
        pBinaryTree->AddToTree(singleWord);
    }
    readFile.close();
}

void WorksOfShakespeare::PrintTreeToFile()
{
    ofstream sortedFile;
    sortedFile.open("./test_data/ShakespearSorted.txt");

    pBinaryTree->PrintTreeToFile(&sortedFile);
    cout << "Printed the complete works of Shakespeare, alphabetically, to the file test_data/ShakespearSorted.txt" << endl;

    sortedFile.close();
}

void WorksOfShakespeare::BuildArrayFromTree()
{
    // Get number of nodes in the tree
    int count = GetNodeCount(); 

    // Allocate a string array large enough for 
    // every word used by Shakespeare 
    pArray = new string[count];

    // Copy the words from the Binary tree to the array
    int index = 0;
    pBinaryTree->ArrayFromTree(pArray, &index);  
}

int WorksOfShakespeare::GetNodeCount()
{
    return pBinaryTree->GetNumberOfNodes();
}

void WorksOfShakespeare::Display()
{
    pBinaryTree->DisplayTreeInOrder();
}
