#include "Node.h"

// constructor
Node::Node(string word)
{
    m_sWord = word;
    m_nInstances = 1;
    m_pLeft = NULL;
    m_pRight = NULL;
}

// node destructor
Node::~Node()
{
    delete (m_pLeft);
    delete (m_pRight);
    m_pLeft = m_pRight = NULL;
}
