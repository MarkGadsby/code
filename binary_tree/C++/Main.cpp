#include "Shakespeare.h"
#include <iostream>
#include <ctime>

int linearSearch(string* array, int length, string item_sought)
{
    int index = -1;
    int i = 0;

    bool found = false;

    while (i < length && found == false)
    {
        if (array[i] == item_sought)
        {
            index = i;
            found = true;
        }
        i++;
    }
    cout << "*\t";
    cout << "Iterations:\t\t" << i << endl;
    return index;
}

int binarySearch(string* array, int length, string item_sought)
{
    int index = -1;

    bool found = false;

    int first = 0;
    int last = length - 1;
    int iterations = 0;

    while (first <= last && found == false)
    {
        int midpoint = (first + last) / 2;

        if (array[midpoint] == item_sought)
        {
            index = midpoint;
            found = true;
        }
        else
        {
            if (array[midpoint] < item_sought)
            {
                first = midpoint + 1;
            }
            else
            {
                last = midpoint - 1;
            }
            iterations++;
        }
    }
    cout << "*\t";
    cout << "Iterations:\t\t" << iterations << endl;
    return index;
}

int main()
{
    WorksOfShakespeare*  pCompleteWorksOfShakespeare = new WorksOfShakespeare();

    pCompleteWorksOfShakespeare->BuildTreeFromWorks();
    pCompleteWorksOfShakespeare->BuildArrayFromTree();
    pCompleteWorksOfShakespeare->PrintTreeToFile();

    string* ShakespearsWordArray = pCompleteWorksOfShakespeare->GetArray();
    int Size = pCompleteWorksOfShakespeare->GetNodeCount();

    clock_t begin = 0;
    clock_t end = 0;
    int indexIfFound = -2;
    int elapsed_ticks = 0;

    string wordInQuestion = "trash";

    cout << endl;
    cout << "****** Statistics for linearSearch ****** " << endl;
    cout << '*' << endl;
    cout << "*\t";
    cout << "Word:\t\t\t" << wordInQuestion << endl;
    begin = clock();
    indexIfFound = linearSearch(ShakespearsWordArray, Size, wordInQuestion);
    end = clock();
    elapsed_ticks = end - begin;
    if (indexIfFound == -1)
    {
        cout << "*\t";
        cout << "Used by Shakespeare:\t" << "False" << endl;
    }
    else
    {
        cout << "*\t";
        cout << "Used by Shakespeare:\t" << "True" << endl;
    }
    cout << "*\t";
    cout << "Elapsed ticks:\t\t" << elapsed_ticks << endl;
    cout << "*" << endl;
    cout << "*****************************************" << endl;
    cout << endl;
    cout << "****** Statistics for binarySearch ****** " << endl;
    cout << '*' << endl;
    cout << "*\t";
    cout << "Word:\t\t\t" << wordInQuestion << endl;
    begin = clock();
    indexIfFound = binarySearch(ShakespearsWordArray, Size, wordInQuestion);
    end = clock();
    elapsed_ticks = end - begin;
    if (indexIfFound == -1)
    {
        cout << "*\t";
        cout << "Used by Shakespeare:\t" << "False" << endl;
    }
    else
    {
        cout << "*\t";
        cout << "Used by Shakespeare:\t" << "True" << endl;
    }
    cout << "*\t";
    cout << "Elapsed ticks:\t\t" << elapsed_ticks << endl;
    cout << "*" << endl;
    cout << "*****************************************" << endl;

//    pCompleteWorksOfShakespeare->FindInTree("Gotten");

    delete pCompleteWorksOfShakespeare;
    return 0;
}
