﻿using Binary_Tree;
BinaryTree tree_of_shakespeare = new BinaryTree();

tree_of_shakespeare.LoadWorks();
tree_of_shakespeare.PrintToScreen();
tree_of_shakespeare.PrintToFile();

Console.WriteLine("Please enter the word you would like to find:");
string word;
do 
{
    word = Console.ReadLine();
    
    if (tree_of_shakespeare.DoSearch(word))
    {
        Console.WriteLine($"The word {word} is found");
    }
    else
    {
        Console.WriteLine($"The word {word} is NOT found");
    }
}
while (word != "Exit");


