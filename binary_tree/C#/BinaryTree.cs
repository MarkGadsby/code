namespace Binary_Tree
{
    class BinaryTree
    {
        public Node? root;

        public BinaryTree()
        {
            root = null;
        }

        public void LoadWorks()
        {
            try
            {
                StreamReader sr = new StreamReader("./../../test_data/Shakespeare.txt");
                
                while (!sr.EndOfStream)
                { 
                    string[] words = sr.ReadLine().Split(new string[] { " ", ",", ", ", "_", "-", "—", "?", ";", ".", "'", ":", "!", "‘", "’", "\"", "“", "”", "(", ")", "[", "]", "{", "}", "*", "/", "&", "#", "$", "|" }, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
                    foreach (string word in words)
                    {
                        if (!char.IsDigit(word[0]))
                        {
                            root = binary_tree_add(root, word);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool DoSearch(string word)
        {
            return binary_tree_search(root, word);
        } 
        
        public void PrintToScreen()
        {
            in_order_traversal(root);
        }

        public void PrintToFile()
        {
            try
            {
                StreamWriter sw = new StreamWriter("./../../test_data/InOrder.txt");
                in_order_traversal(root, sw);
                sw.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void in_order_traversal(Node node, StreamWriter sw)
        {
            if (node == null)
                return;

            in_order_traversal(node.getLeftNode(), sw);
            sw.WriteLine($"{node.getInstances()}\t{node.getWord()}");
            in_order_traversal(node.getRightNode(), sw);
        }

        private void in_order_traversal(Node node)
        {
            if (node == null)
                return;

            in_order_traversal(node.getLeftNode());
            Console.WriteLine($"{node.getInstances()}\t{node.getWord()}");
            in_order_traversal(node.getRightNode());
        }

        private Node binary_tree_add(Node node, string newWord)
        {
            if (node == null)
            {
                node = new Node(newWord);
            }
            else
            {
                int result = String.Compare(newWord, node.getWord()); 

                if (result == 0)
                {
                    node.addInstance();
                }
                else if (result < 0) 
                {
                    node.setLeftNode(binary_tree_add(node.getLeftNode(), newWord));
                }
                else if (result > 0)
                {
                    node.setRightNode(binary_tree_add(node.getRightNode(), newWord));
                }
            }
            return node;
        }

        private bool binary_tree_search(Node node, string search_item)
        {
            if (node == null)
                return false;

            int result = String.Compare(search_item, node.getWord());

            if (result == 0)
                return true;
            else if (result < 0)
                return binary_tree_search(node.getLeftNode(), search_item);
            else if (result > 0)
                return binary_tree_search(node.getRightNode(), search_item);

            return false;            
        }
    }
}