namespace Binary_Tree
{
    class Node
    {
        private Node leftNode;
        private Node rightNode;
        
        private String word;
        private int instances;

        // Constructor 
        public Node(string given_word)
        {
            word = given_word;
            instances = 1;
        }

        // Getters
        public Node getLeftNode()
        {
            return leftNode;
        }

        public Node getRightNode()
        {
            return rightNode;
        }

        public string getWord()
        {
            return word;
        }

        public int getInstances()
        {
            return instances;
        }

        // Setters
        public void setLeftNode(Node givenNode)
        {
            leftNode = givenNode;
        }

        public void setRightNode(Node givenNode)
        {
            rightNode = givenNode;
        }

        // helper
        public void addInstance()
        {
            instances++;
        }
    }
}
