﻿using System.Collections.Generic;

var children = new List<Dictionary<string, List<string>>>()
{
    new Dictionary<string, List<string>>()
    {
        {"name", new List<string>(){"Nadia"}},
        {"dietary", new List<string>(){"Vegetarian"}},
        {"emergency_contact", new List<string>(){"Mr Leigh"}},
        {"days_present", new List<string>(){"Tuesday", "Wednesday", "Thursday"}}
    },   
    new Dictionary<string, List<string>>()
    {
        {"name", new List<string>(){"Nathaniel"}},
        {"dietary", new List<string>(){"None"}},
        {"emergency_contact", new List<string>(){"Ms Clyne"}},
        {"days_present", new List<string>(){"Monday", "Tuesday"}}
    }   
};

children.Add(new Dictionary<string, List<string>>()
{
    {"name", new List<string>(){"Brian"}},
    {"dietary", new List<string>(){"Dairy Intolerant"}},
    {"emergency_contact", new List<string>(){"Mr Clough"}},
    {"days_present", new List<string>(){"Monday", "Friday"}}
});

children.Add(new Dictionary<string, List<string>>()
{
    {"name", new List<string>(){"Edna"}},
    {"dietary", new List<string>(){"Vegetarian"}},
    {"emergency_contact", new List<string>(){"Mrs Everage"}},
    {"days_present", new List<string>(){"Friday"}}
});

Console.WriteLine("\nPrinting dietary needs for the Friday children");
Console.WriteLine("================================================");

for (int c = 0; c < children.Count; c++)
{
    Dictionary<string, List<string>> child = children[c];

    for (int i = 0; i < child["days_present"].Count; i++)
    {
        if (child["days_present"][i] == "Friday")
            Console.WriteLine($"{child["name"][0]} - {child["dietary"][0]}");
    }
}

Console.WriteLine("\n>> Added Friday to Nathaniel's days");

for (int c = 0; c < children.Count; c++)
{
    Dictionary<string, List<string>> child = children[c];

    if (child["name"][0] == "Nathaniel")
    {
        child["days_present"].Add("Friday");
    }
}

Console.WriteLine("\nPrinting dietary needs for the Friday children");
Console.WriteLine("================================================");

for (int c = 0; c < children.Count; c++)
{
    Dictionary<string, List<string>> child = children[c];

    for (int i = 0; i < child["days_present"].Count; i++)
    {
        if (child["days_present"][i] == "Friday")
            Console.WriteLine($"{child["name"][0]} - {child["dietary"][0]}");
    }
}

Console.WriteLine("\nPrinting all the children");
Console.WriteLine("===========================");

for (int c = 0; c < children.Count; c++)
{
    Dictionary<string, List<string>> child = children[c];
    Console.WriteLine($"name = {child["name"][0]} - contact = {child["emergency_contact"][0]}");
}

Console.WriteLine("\n>> Removing Nadia");

for (int c = 0; c < children.Count; c++)
{
    Dictionary<string, List<string>> child = children[c];

    if (child["name"][0] == "Nadia")
    {
        children.Remove(child);
    }
}

Console.WriteLine("\nPrinting all the children");
Console.WriteLine("===========================");

for (int c = 0; c < children.Count; c++)
{
    Dictionary<string, List<string>> child = children[c];
    Console.WriteLine($"name = {child["name"][0]} - contact = {child["emergency_contact"][0]}");
}
Console.WriteLine();