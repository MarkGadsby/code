﻿class Queue
{
    private const int MAX_QUEUE_SIZE = 10;

    // Declare a global array to hold the queue items
    private int[] queue = new int[MAX_QUEUE_SIZE];

    // Declare queue markers
    private int front = 0;
    private int rear = -1;

    //==========================================
    // Print the queue on the screen 
    //==========================================
    public void ShowQueue()
    {
        Console.WriteLine();
        
        for (int i = 0; i < MAX_QUEUE_SIZE; i++)
        {
            Console.Write(queue[i] + "\t");
        }
        Console.Write("\n\n");
    }

    //==========================================
    // Checks if the queue is full by comparing
    // the next slot with the max queue size.
    //==========================================
    private bool isFull()
    {
        return true;
    }

    //==========================================
    // Adds a new item to the end of the queue. 
    // Increments the rear marker.
    //==========================================
    public void enQueue(int new_value)
    {
    }

    //==========================================
    // Checks if the queue is empty by seeing if  
    // the front is past the rear. 
    //==========================================
    private bool isEmpty()
    {
        return true;
    }

    //==========================================
    // Removes and returns the item from the front
    // of the queue. 
    // Increments the front marker.
    //==========================================
    public int deQueue()
    {
        return 0;
    }
}