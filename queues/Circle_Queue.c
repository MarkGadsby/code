// To build this code: gcc -o QueueProg Circle_Queue.c
// To run this code: ./QueueProg

#include "stdio.h"
#include "stdbool.h"

// define the maximum queue size
#define MAX_QUEUE_SIZE 5

// Declare a global array to hold the queue items
int queue[MAX_QUEUE_SIZE];

// Declare queue markers
int front;
int rear;

//==========================================
// Initialise the queue to all zeros
// Initialise the queue markers to zero
//==========================================
void Initialise()
{
    for (int i = 0; i < MAX_QUEUE_SIZE; i++)
    {
        queue[i] = 0;
    }

    front = -1;
    rear = -1;
}

//==========================================
// Print the queue on the screen 
//==========================================
void ShowYourself()
{
    printf("\n");

    for (int i = 0; i < MAX_QUEUE_SIZE; i++)
    {
        printf("\t%i", queue[i]);
    }

    printf("\n");
}

//==========================================
// If the queue is not full, the rear index 
// pointer must be adjusted to reference the 
// next free position so that the new element 
// can be added. Remember that, with a circular 
// queue, the next free position may be position 
// 0 (if the rear index pointer is currently 
// referencing the position at the end of the array).
//==========================================
bool isFull()
{
    if ((rear + 1) % MAX_QUEUE_SIZE == front)
    {
        return true;
    }
    else
    {
        return false;
    }       
}

//==========================================
// Notice that if the item is the first to be
// added to the queue, the front index pointer
// is set to 0.
//==========================================
void enQueue(int new_value)
{
    if (isFull() == true)
    {
        printf("Sorry the queue is full\n");
    }
    else
    {
        rear = (rear + 1) % MAX_QUEUE_SIZE;    
        queue[rear] = new_value;

        if (front == -1)
        {
            front = 0;
        }
    }
}

//==========================================
//
//
//==========================================
bool isEmpty()
{
    return true;
}

//==========================================
// 
// 
//==========================================
int deQueue()
{
    int return_value = 0;
    return return_value;
}

//==========================================
// main()
//==========================================
void main()
{
    Initialise();
    ShowYourself();
}