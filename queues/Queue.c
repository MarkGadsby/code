// To build this code: gcc -o QueueProg Queue.c
// To run this code: ./QueueProg

#include "stdio.h"
#include "stdbool.h"

// define the maximum queue size
#define MAX_QUEUE_SIZE 5

// Declare a global array to hold the queue items
int queue[MAX_QUEUE_SIZE];

// Declare queue markers
int front;
int rear;

//==========================================
// Initialise the queue to all zeros
// Initialise the queue markers
//==========================================
void Initialise()
{
    for (int i = 0; i < MAX_QUEUE_SIZE; i++)
    {
        queue[i] = 0;
    }

    front = 0;
    rear = -1;
}

//==========================================
// Print the queue on the screen 
//==========================================
void ShowYourself()
{
    printf("\n");

    for (int i = 0; i < MAX_QUEUE_SIZE; i++)
    {
        printf("\t%i", queue[i]);
    }

    printf("\n");
}

//==========================================
// Checks if the queue is full by comparing
// the next slot with the max queue size.
//==========================================
bool isFull()
{
    if (rear + 1 == MAX_QUEUE_SIZE)
    {
        return true;
    }
    else
    {
        return false;
    }       
}

//==========================================
// Adds a new item to the end of the queue. 
// Increments the rear marker.
//==========================================
void enQueue(int new_value)
{
    if (isFull() == true)
    {
        printf("Sorry the queue is full\n");
    }
    else
    {
        rear++; 
        queue[rear] = new_value;
    }
}

//==========================================
// Checks if the queue is empty by seeing if  
// the front is past the rear. 
//==========================================
bool isEmpty()
{
    if (front > rear)
    {
        return true;
    }
    else
    {
        return false;
    }
}

//==========================================
// Removes and returns the item from the front
// of the queue. 
// Increments the front marker.
//==========================================
int deQueue()
{
    int return_value = 0;

    if (isEmpty() == true)
    {
        printf("Sorry the queue is empty\n");
    }
    else
    {
        return_value = queue[front];
        queue[front] = 0;
        front++;
    }
    return return_value;
}

//==========================================
// main()
//==========================================
void main()
{
    Initialise();
    ShowYourself();
}