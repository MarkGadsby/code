﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Linked List Program");

PriorityQueue myQueue = new PriorityQueue(); // instantiate a new priority queue

myQueue.enqueue("Hattie", 14);
myQueue.enqueue("Barbara", 9);
myQueue.enqueue("Sidney", 19);
myQueue.enqueue("Kenneth", 25);
myQueue.enqueue("Charles", 22);
myQueue.enqueue("Leslie", 4);

Console.WriteLine();

Console.WriteLine(myQueue.dequeue());
Console.WriteLine(myQueue.dequeue());
Console.WriteLine(myQueue.dequeue());
Console.WriteLine(myQueue.dequeue());
Console.WriteLine(myQueue.dequeue());
Console.WriteLine(myQueue.dequeue());
Console.WriteLine(myQueue.dequeue());

class Node
{
    private int data;
    private Node next;

    public Node(int given_data)
    {
        data = given_data;
        next = null;
    }

    public void set_next(Node old_head)
    {
        next = old_head;
    }

    public int get_data()
    {
        return data;
    }

    public Node get_next()
    {
        return next;
    }
}

class Linked_List
{
    private Node head;

    public Linked_List()
    {
        head = null;
    }

    public void insert_at_front(int new_entry)
    {
        Node new_node = new Node(new_entry);

        // insert at front of the list
        if (head == null)
            head = new_node;
        else    
        {
            new_node.set_next(head);
            head = new_node;
        }
    }

    public void insert_in_order(int new_entry)
    {
        Node new_node = new Node(new_entry);
        Node current_node = head;

        if (current_node == null) 
            head = new_node;  
        else if (current_node.get_data() >= new_node.get_data())
        {
            new_node.set_next(head);
            head = new_node;
        }
        else
        {
            while (current_node.get_next() != null && current_node.get_next().get_data() < new_node.get_data())
            {
                current_node = current_node.get_next();
            }
            new_node.set_next(current_node.get_next());
            current_node.set_next(new_node);
        }
    }

    public void traverse_list()
    {
        Node current_node = head;
        while (current_node != null)
        {
            Console.WriteLine(current_node.get_data());
            current_node = current_node.get_next();
        }
    } 
}

class PriorityNode
{
    private string data;
    private int priority;
    private PriorityNode next;

    public PriorityNode(string given_data, int given_priority)
    {
        data = given_data;
        priority = given_priority;
        next = null;
    }

    public void set_next(PriorityNode old_head)
    {
        next = old_head;
    }

    public string get_data()
    {
        return data;
    }

    public PriorityNode get_next()
    {
        return next;
    }

    public int get_priority()
    {
        return priority;
    }

}

class PriorityQueue
{
    private PriorityNode front;
    private PriorityNode rear;

    public PriorityQueue()
    {
        front = null;
        rear = null;
    }

    public void setFront(PriorityNode newFront)
    {
        front = newFront;
    }

    public PriorityNode getFront()
    {
        return front;
    }

    public void setRear(PriorityNode newRear)
    {
        front = newRear;
    }

    public PriorityNode getRear()
    {
        return rear;
    }

    public void enqueue(string data, int priority)
    {
        PriorityNode new_node = new PriorityNode(data, priority);

        if (front == null)  // Queue is empty
        {
            front = new_node;
            rear = new_node;
        }
        else
        {
            if (new_node.get_priority() > front.get_priority())
            {
                new_node.set_next(front);
                front = new_node;
            }                
            else if (new_node.get_priority() <= rear.get_priority())
            {
                rear.set_next(new_node);
                rear = new_node;
            }
            else
            {
                PriorityNode current_node = front;
                PriorityNode previous_node = null;
                while (current_node.get_priority() >= new_node.get_priority())
                {
                    previous_node = current_node;
                    current_node = current_node.get_next();
                }
                new_node.set_next(current_node);
                previous_node.set_next(new_node); 
            }
        }
    }

    public string dequeue()
    {
        string retValue = "";

        if (front == null)
            Console.WriteLine("Nothing dequeued - queue is empty");
        else
        {
            retValue = front.get_data();
            front = front.get_next();
        }

        return retValue;
    }

    public void traverse()
    {
        PriorityNode current_node = front;

        while (current_node != null)
        {
            Console.WriteLine(current_node.get_data());
            current_node = current_node.get_next();
        }
    } 
}