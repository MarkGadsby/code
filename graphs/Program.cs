﻿// Each key is a node in the graph.
// Each value is a list of the node's neighbours
var testGraph = new Dictionary<string, List<string>>() 
{
    {"1", new List<string>() {"2", "9"} },
    {"2", new List<string>() {"1"} },
    {"3", new List<string>() {"4", "5", "6", "9"} },
    {"4", new List<string>() {"3"} },
    {"5", new List<string>() {"3", "8"} },
    {"6", new List<string>() {"3", "7"} },
    {"7", new List<string>() {"6", "8"} },
    {"8", new List<string>() {"5", "7"} },
    {"9", new List<string>() {"1", "3", "7"} }
};

Console.WriteLine("### Graph traversal - breadth-first search (BFS) ###");

// Search for a value and return true if it has been found
string itemToFind = "8";
string start = "1";
bool found = BreadthFirstSearch(testGraph, start, itemToFind);

// Output the search result
Console.WriteLine($"\nThe target node is {itemToFind}");

if (found == true)
    Console.WriteLine($"{itemToFind} was found in the graph");
else
    Console.WriteLine($"{itemToFind} was NOT found in the graph");

// A breadth-first search performed on a graph stored as a dictionary
static bool BreadthFirstSearch(Dictionary<string, List<string>> graph,string startNode, string targetNode)
{    
    List<string> myDiscoveredList = new List<string>();
    Queue<string> myQueue = new Queue<string>();

    myDiscoveredList.Add(startNode);
    myQueue.Enqueue(startNode);
    bool found = false;

    while (myQueue.Count > 0 && found == false)
    {        
        string currentNode = myQueue.Dequeue();
        List<string> neighbours = new List<string>();

        // Get the current node's list of neighbours
        neighbours = graph[currentNode];

        bool inDiscovedList = false;

        foreach (string node in neighbours) 
        {            
            // Check if the node has not already been discovered
            foreach (string discoveredNode in myDiscoveredList)
            {
                if (node == discoveredNode)
                    inDiscovedList = true;
            }
            // if not in discovered list 
            if (!inDiscovedList)
            {
                if (node == targetNode)
                    found = true;
                else 
                {
                    myDiscoveredList.Add(node);
                    myQueue.Enqueue(node);
                }
            }
            inDiscovedList = false;
        }
    }
    return found;
}

