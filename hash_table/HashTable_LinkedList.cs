using System;
using System.Collections.Generic;
using System.Text;

namespace Hashing
{
    class HashEntryLink
    {
        private string data;
        private HashEntryLink next;

        // constructor
        public HashEntryLink(string given_data)
        {
            data = given_data;
            next = null;
        }

        // getters
        public string get_data()
        {
            return data;
        }

        public HashEntryLink get_next()
        {
            return next;
        }

        public void set_next(HashEntryLink given_next)
        {
            next = given_next;
        }
    }

    class HashTableLink
    {
        const int table_size = 10;

        HashEntryLink[] hash_table;

        public HashTableLink()
        {
            hash_table = new HashEntryLink[table_size];

            for (int i = 0; i < hash_table.Length; i++)
            {
                hash_table[i] = null;
            }
        }

        // method to add the ordinal values the chars in the id string together.
        private int GenerateHashKey(string id)
        {
            int return_value = 0;

            for (int i = 0; i < id.Length; i++)
            {
                return_value += id[i];
            }
            return return_value % table_size;
        }

        private bool HaveSpace()
        {
            bool return_value = false;

            for (int i = 0; i < hash_table.Length; i++)
            {
                if (hash_table[i] == null)
                {
                    return_value = true;
                }
            }
            return return_value;
        }

        public void Insert(string data)
        {
            // Instantiate new link
            HashEntryLink newLink = new HashEntryLink(data);

            int hash_key = GenerateHashKey(data);

            if (hash_table[hash_key] == null) // if empty slot
            {
                hash_table[hash_key] = newLink;
            }
            else
            {
                HashEntryLink runner = hash_table[hash_key];

                // run to end of list but keep checking for data being already present
                while (runner.get_data() != data && runner.get_next() != null)
                {
                    runner = runner.get_next();
                }

                // add link - if not there already
                if (runner.get_data() != data)
                    runner.set_next(newLink);
            }
        }

        public int Locate(string data)
        {
            int hash_key = GenerateHashKey(data);

            // if an emply slot return -1
            if (hash_table[hash_key] == null)
                return -1;

            HashEntryLink runner = hash_table[hash_key];

            // traverse the list while there is no data match
            while (runner.get_data() != data && runner.get_next() != null)
            {
                runner = runner.get_next();
            }
            return hash_key;
        }

        public void DisplayTable()
        {
            Console.WriteLine("\n\tHash Table:");
            Console.WriteLine("\t==========");

            for (int i = 0; i < hash_table.Length; i++)
            {
                Console.Write($"[{i}]");

                if (hash_table[i] != null)
                {
                    HashEntryLink runner = hash_table[i];
                    Console.Write($"\t{runner.get_data()}");

                    while (runner.get_next() != null)
                    {
                        runner = runner.get_next();
                        Console.Write($"\t{runner.get_data()}");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
