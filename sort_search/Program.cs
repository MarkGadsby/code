﻿using System;

namespace seaching_sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            SortingAlgorithms sorter = new SortingAlgorithms();

            Console.WriteLine("MergeSort");
            sorter.MuddleArray();
            sorter.Print();
            sorter.MergeSort();
            sorter.Print();
            Console.WriteLine();
        }
    }
}