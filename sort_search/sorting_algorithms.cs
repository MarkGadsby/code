using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace seaching_sorting
{
    class SortingAlgorithms
    {
        private int[] number_array;

        public SortingAlgorithms()
        {
            number_array = new int[7]; 
        }

        public void MuddleArray()
        {
            if (number_array.Length == 7)
            {
                number_array[0] = 8;
                number_array[1] = 100;
                number_array[2] = 1;
                number_array[3] = 18;
                number_array[4] = 12;
                number_array[5] = 55;
                number_array[6] = 200;
            }
        }

        public void RandomiseArray()
        {
            Random rand = new Random();

            if (number_array.Length == 7)
            {
                for(int i = 0; i <  number_array.Length; i++)
                {
                    number_array[i] = rand.Next(0,100);
                }                
            }
        }

        public void Print()
        {
            Console.Write("\t [ ");

            foreach (int i in number_array)
                Console.Write($"{i} ");

            Console.WriteLine("]");
        }   

        public void Print(int[] array, ConsoleColor colour)
        {
            ConsoleColor orig = Console.ForegroundColor;
            Console.ForegroundColor = colour;
            Console.Write("\t [ ");

            foreach (int i in array)
                Console.Write($"{i} ");

            Console.WriteLine("]");
            Console.ForegroundColor = orig;
        }   

        public void BubbleSort_I()
        {
            int temp_int = 0;

            for (int pass = 0; pass < number_array.Length - 1; pass++)
            {
                for (int comp = 0; comp < number_array.Length - 1; comp++)
                {
                    // Check for need to swap
                    if (number_array[comp] > number_array[comp + 1])
                    {
                        // swap
                        temp_int = number_array[comp];
                        number_array[comp] = number_array[comp + 1];
                        number_array[comp + 1] = temp_int;
                    }
                }
            }       
        }  

        public void BubbleSort_II()
        {
            int temp_int = 0;

            for (int pass = 0; pass < number_array.Length - 1; pass++)
            {
                for (int comp = 0; comp < number_array.Length - 1 - pass; comp++)
                {
                    // Check for need to swap
                    if (number_array[comp] > number_array[comp + 1])
                    {
                        // swap
                        temp_int = number_array[comp];
                        number_array[comp] = number_array[comp + 1];
                        number_array[comp + 1] = temp_int;
                    }
                }
            }       
        }  

        public void BubbleSort_III()
        {
            int temp_int = 0;
            bool swapped = true;
            int pass = 0;

            while (pass < number_array.Length - 1 && swapped == true)
            {
                swapped = false;
                for (int comp = 0; comp < number_array.Length - 1 - pass; comp++)
                {
                    // Check for need to swap
                    if (number_array[comp] > number_array[comp + 1])
                    {
                        // swap
                        temp_int = number_array[comp];
                        number_array[comp] = number_array[comp + 1];
                        number_array[comp + 1] = temp_int;
                        swapped = true;
                    }
                }
                pass++;                
            }
        } 

        public void InsertionSort()
        {
            int num_items = number_array.Length;

            for (int i = 1; i < num_items; i++)
            {
                int current_value = number_array[i];
                int position = i;
                while (position > 0 && number_array[position -1] > current_value)
                {
                    // move the number larger then current_value to the right 
                    number_array[position] = number_array[position -1];
                    // make our way to left
                    position--;
                }
                number_array[position] = current_value;
            }
        }

        public void MergeSort()
        {
            number_array = MergeSort(number_array);
        }

        private int[] MergeSort(int[] array)
        {
            Print(array, ConsoleColor.Green);

            // Base case
            if (array.Length == 1)
                return array;

            int midpoint = (array.Length - 1) / 2; 

            int leftSize = midpoint + 1; 

            int rightSize; 

            if (array.Length % 2 == 0) 
                rightSize = midpoint + 1; 
            else 
                rightSize = midpoint; 

            int[] left = new int[leftSize]; 
            int[] right = new int[rightSize]; 

            for (int i = 0; i < leftSize; i++) 
            {
                left[i] = array[i];
            }

            int indexItems = midpoint + 1;

            for (int i = 0; i < rightSize; i++) 
            {
                right[i] = array[indexItems];
                indexItems++;
            }

            left = MergeSort(left); 
            right = MergeSort(right); 

            return Merge(left, right); 
        }

        private int[] Merge(int[] left, int[] right) 
        {
            int indexLeft = 0; 
            int indexRight = 0; 
            int indexMerged = 0;

            int[] merged = new int[left.Length + right.Length]; 

            while (indexLeft < left.Length && indexRight < right.Length) 
            {
                if (left[indexLeft] < right[indexRight])
                {
                    merged[indexMerged] = left[indexLeft];
                    indexLeft++;
                    indexMerged++;
                }
                else 
                {
                    merged[indexMerged] = right[indexRight];
                    indexRight++;
                    indexMerged++;
                }
            }

            while (indexLeft < left.Length)
            {
                merged[indexMerged] = left[indexLeft];
                indexLeft++;
                indexMerged++;
            }

            while (indexRight < right.Length) 
            {
                merged[indexMerged] = right[indexRight];
                indexRight++;
                indexMerged++;
            }

            Print(merged, ConsoleColor.Yellow);
            return merged;
        }

        public void QuickSort(int start, int end)
        {
            if (start < end)
            {
                // partition the list
                int split = Partition(start, end);
                // sort both halves
                QuickSort(start, split - 1);
                QuickSort(split + 1, end);
            }
        }

        private int Partition(int start, int end)
        {
            int pivot = number_array[start];
            int leftmark = start + 1;  
            int rightmark = end;
            bool done = false;

            int temp = 0;

            while (done == false)
            {
                // TODO:
            }
            
            // swap the pivot with number_array[rightmark];
            temp = number_array[start];
            number_array[start] = number_array[rightmark];
            number_array[rightmark] = temp;
            return rightmark;
        }
    }
}
