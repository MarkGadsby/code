using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace seaching_sorting
{
    class SeachingAlgorithms
    {
        private int[] number_array;

        public SeachingAlgorithms()
        {
            number_array = new int[7]; 
        }

        public void MuddleArray()
        {
            if (number_array.Length == 7)
            {
                number_array[0] = 8;
                number_array[1] = 100;
                number_array[2] = 1;
                number_array[3] = 18;
                number_array[4] = 12;
                number_array[5] = 55;
                number_array[6] = 200;
            }
        }

        public void RandomiseArray()
        {
            Random rand = new Random();

            if (number_array.Length == 7)
            {
                for(int i = 0; i <  number_array.Length; i++)
                {
                    number_array[i] = rand.Next(0,100);
                }                
            }
        }

        public void Print()
        {
            Console.Write("\t [ ");

            foreach (int i in number_array)
                Console.Write($"{i} ");

            Console.WriteLine("]");
        }      


        public int LinearSearch_I(int num)
        {
            int foundIndex = -1;

            for (int i = 0; i < number_array.Length; i++)
            {
                if (number_array[i] == num)
                    foundIndex = i;
            } 
            return foundIndex;
        }

        public int LinearSearch_II(int num) 
        {
            int found_index = -1;
    
            int i = 0;
            bool found = false;

            // Repeat while the end of the array has not been reached
            // and the search item has not been found
            while (i < number_array.Length && found == false)
            {
                if (number_array[i] == num)
                {
                    found_index = i;
                    found = true;
                }
                i++;
            }
            return found_index;
        }

        public int BinarySearch_I(int searchItem)
        {
            bool found = false;
            int foundIndex = -1;
            int first = 0;
            int last = number_array.Length - 1;
            int midpoint = 0;

            // Repeat while there are still items between first and last and the search item has not been found
            while (first <= last && found == false) 
            {
                // Find the midpoint position (in the middle of the range)
                midpoint = (first + last) / 2;

                // Compare the item at the midpoint to the search item
                if (number_array[midpoint] == searchItem) 
                {      
                        foundIndex = midpoint;    // If the item has been found, store the midpoint position
                        found = true;             // Raise the flag to stop the loop
                }
                // Check if the item at the midpoint is less than the search item
                else if (number_array[midpoint] < searchItem) 
                    first = midpoint + 1;
                else 
                    last = midpoint - 1;
            }

            // Return the position of the searchItem or -1 if not found
            return foundIndex;
        }
        
        public int BinarySearch_II(int first, int last, string searchItem)
        {
            // Base case for recursion:
            // The recursion will stop when the index of the first item is greater than the index of last
            if (first > last)
                return -1; // Return -1 if the search item is not found
            else // Recursively call the function
            {
                // Find the midpoint position (in the middle of the range)
                int mid_point = (first + last) / 2;

                Console.WriteLine($"Frist = {first} last = {last} mid = {mid_point}");
                Console.WriteLine($"Is it {number_array[mid_point]}?");

                // Compare the item at the midpoint to the search item
                int comparator = searchItem.CompareTo(number_array[mid_point]);

                if (comparator == 0)
                    return mid_point;
                else if (comparator < 0)
                    return BinarySearch_II(first, --mid_point, searchItem);                        
                else
                    return BinarySearch_II(++mid_point, last, searchItem);                        
            }
        }

    }
}