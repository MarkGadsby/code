# Turtle Keyboard Control #

### This is a python project that demonstrates a way of controlling the turtle (as in Turtle Graphics) by either arrow keyboard presses or button clicks.

![Screenshot](Screenshot.png)

* Please download the code by clicking the three dots in the top right hand corner and selecting Download repository. 
* It uses the [`tkinter`](https://docs.python.org/3/library/tkinter.html#module-tkinter) package (“Tk interface”) that is a Graphical User Interface (GUI) toolkit.
* I use this because it has keyboard binding built in.
* Can you:
 - Change the line width?
 - Change how many pixels are drawn at each press?
 - Change the background colour?
 - Change anything else? 