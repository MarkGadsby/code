﻿const int COL = 4;
const int ROW = 4;

char [,] array_2D = new char[COL,ROW];

array_2D[0,0]  = 'M';
array_2D[0,1]  = 'a';
array_2D[0,2]  = 'r';
array_2D[0,3]  = 'k';
array_2D[1,0]  = 'I';
array_2D[1,1]  = 'v';
array_2D[1,2]  = 'o';
array_2D[1,3]  = 'r';
array_2D[2,0]  = 'C';
array_2D[2,1]  = 'a';
array_2D[2,2]  = 'm';
array_2D[2,3]  = ' ';
array_2D[3,0]  = 'E';
array_2D[3,1]  = 'z';
array_2D[3,2]  = 'r';
array_2D[3,3]  = 'a';

Console.WriteLine();
Console.WriteLine();

Console.WriteLine("2D Array output");
Console.WriteLine("===============");

for (int col = 0; col < COL; col++)
{
    Console.Write(array_2D[2, col]);
}

Console.WriteLine();
Console.WriteLine();

for (int row = 0; row < ROW; row++)
{
    for (int col = 0; col < COL; col++)
    {
        Console.Write(array_2D[row, col]);
    }
    Console.WriteLine();
}

Console.WriteLine();
    
array_2D[2, 0] = 'E';
array_2D[2, 1] = 'v';
array_2D[2, 2] = 'a';
array_2D[2, 3] = 'n';

for (int row = 0; row < ROW; row++)
{
    for (int col = 0; col < COL; col++)
    {
        Console.Write(array_2D[row, col]);
    }
    Console.WriteLine();
}

Console.WriteLine();