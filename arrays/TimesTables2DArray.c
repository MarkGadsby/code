// to build: gcc -o TimesTable  
// to run ./TimesTable
#include <stdio.h>      // Import library for standard input/output functionality

int numbers[10][10];    // 2D array for storing the products

void main()
{
    // Nested for loops to fill the number 2D array    
    for (int row = 0; row < 10; row++)
    {
        for (int col = 0; col < 10; col++)
        {
          numbers[row][col] = (row + 1) * (col + 1);
        }
    }

    // Nested for loops to access and print the 2D array    
    for (int row = 0; row < 10; row++)
    {
        for (int col = 0; col < 10; col++)
        {
            printf("%i", numbers[row][col]);
            printf("\t"); // tab to make the table look neater
      }
      printf("\n\n"); // two new lines after each row
    }
}