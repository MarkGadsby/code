﻿using System;

namespace SpellingWords
{
    class Program
    {
        static void Main(string[] args)
        {
            HeatcoteQ2();
        }

        static void HeatcoteQ2()
        {
            var rand = new Random();
            int [,]results = new int[5, 3];

            for (int student = 0; student < 5; student++)
            {
                for (int test = 0; test < 3; test++)
                {
                    results[student, test] = rand.Next(0, 10);
                }
            }

            for (int student = 0; student < 5; student++)
            {
                for (int test = 0; test < 3; test++)
                {
                    Console.Write(results[student,test] + "\t");                        
                }
                Console.WriteLine();                        
            }
        }

        static void HeatcoteQ1()
        {
            int [] data = new int[100];

            var rand = new Random();
            
            for (int i = 0; i < 100; i++)
            {
                data[i] = rand.Next(1500, 4000);
            }
        
            int sumOfWeights = 0;
            int sumOfLowWeights = 0;
            int numLowWeights = 0;

            for (int i = 0; i < 100; i++)
            {
                sumOfWeights += data[i];
            }

            float averageWeight = sumOfWeights / 100;

            Console.WriteLine("The average weight of babies is: " + averageWeight);                        
            Console.WriteLine("The below average weight babies are:");                        

            for (int i = 0; i < 100; i++)
            {
                if (data[i] < averageWeight - 500)
                {
                    sumOfLowWeights += data[i];
                    numLowWeights++;
                    Console.WriteLine(data[i]);                        
                }
            }

            float averageLowWeight = sumOfLowWeights / numLowWeights;
            Console.WriteLine("The average low weight of babies is: " + averageLowWeight);                        
        }

        static void SpellingWords()
        {
            const int LEVEL_I   = 0; 
            const int LEVEL_II  = 1; 
            const int LEVEL_III = 2; 

            // Create an array of ten elements, and add values later
            string[,] spelling_words = new string[3,10];

            spelling_words [0,0] = "school";
            spelling_words [0,1] = "pull";
            spelling_words [0,2] = "where";
            spelling_words [0,3] = "said";
            spelling_words [0,4] = "today";
            spelling_words [0,5] = "they";
            spelling_words [0,6] = "push";
            spelling_words [0,7] = "house";
            spelling_words [0,8] = "love";
            spelling_words [0,9] = "your";

            spelling_words [1,0] = "path";     
            spelling_words [1,1] = "floor";
            spelling_words [1,2] = "sugar";
            spelling_words [1,3] = "because";
            spelling_words [1,4] = "beautiful";
            spelling_words [1,5] = "clothes";
            spelling_words [1,6] = "whole";
            spelling_words [1,7] = "behind";
            spelling_words [1,8] = "move";
            spelling_words [1,9] = "busy";

            spelling_words [2,0] = "accident";
            spelling_words [2,1] = "answer";
            spelling_words [2,2] = "eight";
            spelling_words [2,3] = "group";
            spelling_words [2,4] = "length";
            spelling_words [2,5] = "minute";
            spelling_words [2,6] = "popular";
            spelling_words [2,7] = "question";
            spelling_words [2,8] = "reign";
            spelling_words [2,9] = "various";

            for (int word = 0; word < 10; word++)
            {
                Console.Write(spelling_words[LEVEL_I, word]);
                Console.Write(" ");
            }

            Console.Write("\n");

            for (int word = 0; word < 10; word++)
            {
                Console.Write(spelling_words[LEVEL_II, word]);
                Console.Write(" ");
            }

            Console.Write("\n");

            for (int word = 0; word < 10; word++)
            {
                Console.Write(spelling_words[LEVEL_III, word]);
                Console.Write(" ");
            }
        }
    }
}