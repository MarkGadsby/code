# Queues and Stacks #

### Here is the base code for the coding challenge due 11th January 2021. ###

* You will recognise the queue code as I copied it from the circular queue 'C' code from last term - remember all that enqueue and dequeue fun? 
* I have put it all in a class called Queue and converted the project to C#.
* I have also added a Stack class.
* Please download the code by clicking the three dots in the top right hand corner and selecting Download repository. 
* Run it up on your device at home in Visual Studio Code. **Any problems contact me via MS Teams**. 
* Please read the chapter in Heathcote on the **stack data type** and refer to the stack class code for an implementation of the push and pop methods.  
* Your mission is to add a procedure (which could be called from the Main() function) for reversing the elements of a queue **with the aid of a stack.** 
* You must submit the code for your procedure via **MS Teams ONLY (please don't e-mail it to me) - it is the deliverable for the set assignment - 'Stack Independent Learning'**.





